$Id

/* DESCRIPTION */

Views SimpleSpy provides a display style plugin for the Views module.
It will take the results and display them as a JQuery SimpleSpy.


/* INSTALATION */

1. Place the views_simplespy module in your modules directory (usually under /sites/all/modules/).
2. Go to /admin/build/modules, and activate the module (you will find it under the Views section).


/* USING VIEWS SIMPLESPY MODULE */

Your view must meet the following requirements:
  * Row style must be set to Fields

Choose Views SimpleSpy in the Style dialog within your view, which will prompt you to configure:
  * Transition time: How fast you want the transition to wait before progressing to the next item, in milliseconds.
    Default is 4000 milliseconds (4 seconds).
  * Visible Items: How many items you want displayed at once. This number should be greater than 0 but less
    than the "Items to Display" for the View.

/* THEMING INFORMATION */

This module comes with a default style, which you can disable in the options (see above). Files included:
  * views-simplespy.css - A default stylesheet with how the classes the author thought would be best used.
  * views-view-simplespy.tpl.php - copy/paste into your theme directory - please see the comments in this file for requirements/instructions.

Both files are commented to explain how things work. Do read them to speed things up.


/* ABOUT THE AUTHOR */

Views SimpleSpy module was created by Justus Boatright
http://drupal.org/user/285635

with SimpleSpy technique by Remy Sharp
http://jqueryfordesigners.com/simple-jquery-spy-effect/
