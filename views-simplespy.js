// $Id
Drupal.behaviors.views_simplespy = function(context) {
	
  if (Drupal.settings.views_simplespy) {
	  $.each(Drupal.settings.views_simplespy, function(id) {
		  var contentClass = 'simplespy-content';
		  var wrapperClass = 'views-simplespy';
		
		  /*
		   * Our view settings
		   */
		  var limit = this.limit; // how many items to display at once
		  var interval = this.interval; // how fast (in milliseconds) the spy repeats
				
		  // the selectors we have to play with
		  var contentSelector = 'ul.' + contentClass; // used for selecting simplespy content list
			var displaySelector = 'div.' + wrapperClass; // used to grab anything under our view
		
			(function ($) {

			$.fn.simpleSpy = function (limit, interval) {
				limit = limit || 4;
				interval = interval || 4000;

				return this.each(function () {
					// 1. setup
					  // capture a cache of all list items
					  // chop list down to limit li elements
					var $list = $(this),
					    items = [], // uninitialised
					    currentItem = parseInt(limit) + 1,
					    total = 0, // initialise later on
					    height = $list.find('> li:first').height();

					// capture the cache
					$list.find('> li').each(function () {
						items.push('<li class="' + $(this).attr('class') + '">' + $(this).html() + '</li>');
					});
					
					total = items.length;
					
					$list.find('> li').filter(':gt(' + (limit - 1) + ')').remove();
					
          // set height of wrapper div for improved performance
					newHeight = $(displaySelector).outerHeight(true);
					$(displaySelector).css({ height : newHeight });

					// 2. effect
					function spy() {
						// insert new item with opacity and height of zero
						var $insert = $(items[currentItem]).css({
							height : 0,
							opacity : 0,
							display : 'none'
						}).prependTo($list);

						// fade the LAST item out
						$list.find('> li:last').animate({ opacity : 0}, 1000, function () {
							// increase the height of the NEW first item
							$insert.animate({ height : height }, 1000).animate({ opacity : 1 }, 1000);

							// finally we can remove the last item
							$(this).remove();
						});

						currentItem++;
						if (currentItem >= total) {
							currentItem = 0;
						}

						setTimeout(spy, interval);
					}

					spy();
				});

			};

			})(jQuery);
		
		
		  $(contentSelector).simpleSpy(limit, interval);
   	});
	
  };

}

