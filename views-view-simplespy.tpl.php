<?php
// $Id
/**
 * @file
 * Displays the items of the simpleSpy.
 *
 * @ingroup views_templates
 *
 *  Note that the simplespy NEEDS <?php print $row ?> to be wrapped by an li element, or it will hide all fields on all rows under the first field.
 * 
 *  The current div wrapping the list gets two classes, which should be enough for most cases
 *     "views-simplespy"
 *     "item-list"
 * 
 *  The ul class must also have the "simplespy-content" class in order for the effect to work properly.
 *
 */
?>
<div class="item-list views-simplespy">
  <?php if (!empty($title)): ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <ul class="simplespy-content">
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print $classes[$id] ?>">
				<?php print $row ?>
			</li>
    <?php endforeach; ?>
  </ul>
</div>
