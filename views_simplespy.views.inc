<?php
// $Id
/**
 * @file
 * Provide an SimpleSpy style plugin for Views. This file is autoloaded by views.
 */


/**
 * Implementation of hook_views_plugins
 *
 *
 */
function views_simplespy_views_plugins() {
	return array(
		'style' => array( // declare the views_simplespy style plugin
			'views_simplespy' => array(
				'title' => t('SimpleSpy'),
				'theme' => 'views_view_simplespy',
				'help' => t('Displays fields using the SimpleSpy effect.'),
				'handler' => 'views_simplespy_plugin_style',
				'uses row plugin' => TRUE,
				'uses fields' => TRUE,
				'uses options' => TRUE,
				'type' => 'normal',
			)
		)
	);
}

