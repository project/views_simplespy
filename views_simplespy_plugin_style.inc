<?php
// $Id

/**
 * @file
 * Provide an SimpleSpy style plugin for Views. This file is autoloaded by views.
 */

/**
  * Implementation of views_plugin_style().
  */
class views_simplespy_plugin_style extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['limit'] = array('default' => 4);
    $options['interval'] = array('default' => 4000);
    return $options;
	}
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Find out how many items the display is currently configured to show (limit)
    $maxitems = $this->display->handler->get_option('items_per_page');

    $form['limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Visible Items'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->options['limit'],
      '#required' => TRUE,
      '#element_validate' => array('simplespy_limit_validate'),
      '#description' => t('The number of items to display at once. This number <em>must be less than items to display</em> for this view and <em>must be higher than 0</em>. Do not use commas in this field.'),
    );
    $form['interval'] = array(
      '#type' => 'textfield',
      '#title' => t('Transition time'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->options['interval'],
      '#field_suffix' => t('Milliseconds'),
      '#required' => TRUE,
      '#element_validate' => array('simplespy_interval_validate'),
      '#description' => t('The time in milliseconds (1000 ms = 1 sec) between each row transition. The value <em>must be higher than 0</em>. Do not use commas in this field.'),
    );
    $form['include-style'] = array(
      '#type' => 'checkbox',
      '#title' => t("Use the module's default styling"),
      '#default_value' => $this->options['include-style'],
      '#description' => t("If you disable this, the file in the module's directory <em>views-simplespy.css</em> will not be loaded"),
    );

  }

  /**
   * Render the display in this style.
   */
  function render() {
    $output = parent::render();

    // dpm($this->display->handler->get_option('fields')); // for dev-troubleshooting
    // Preparing the js variables and adding the js to our display
    // we do it here so we dont have it run once every group
    drupal_add_js(drupal_get_path('module', 'views_simplespy') .'/views-simplespy.js');
    $view_settings['limit'] = $this->options['limit'];
    $view_settings['interval'] = $this->options['interval'];
    $view_id = 'views-simplespy-'. $this->view->name .'-'. $this->view->current_display;

    drupal_add_js(array('views_simplespy' => array($view_id => $view_settings)), 'setting');

    return $output;
  }	
}

/** TODO: join these validation functions into one, cleaner function. */

/**
  * Validates the options entered
  */
function simplespy_limit_validate($element, &$form_state) {
  /* must be numeric value */
  if(!is_numeric($element['#value'])) {
    form_error($element, ('<em>'. ($element['#title'] . t('</em> must be a <em>numberic</em> value.'))));
  }

  /* dont allow commas */
  if (strstr($element['#value'], ',')) {
    form_error($element, t('Please use a "." instead of a "," in the <em>') . $element['#title'] . t('</em> field.'));
  }

  /* must be higher than 0 */
  if ($element['#value'] <= 0) {
    form_error($element, ('<em>'. ($element['#title'] . t('</em> must be a <em>number</em> higher than 0.<br />If you would like it to be instanteneous, enter 0.001'))));
  }
}

/**
  * Validates the options entered
  */
function simplespy_interval_validate($element, &$form_state) {
  /* must be numeric value */
  if(!is_numeric($element['#value'])) {
    form_error($element, ('<em>'. ($element['#title'] . t('</em> must be a <em>numberic</em> value.'))));
  }

  /* dont allow commas */
  if (strstr($element['#value'], ',')) {
    form_error($element, t('Please use a "." instead of a "," in the <em>') . $element['#title'] . t('</em> field.'));
  }

  /* must be higher than 0 */
  if ($element['#value'] <= 0) {
    form_error($element, ('<em>'. ($element['#title'] . t('</em> must be a <em>number</em> higher than 0.<br />If you would like it to be instanteneous, enter 0.001'))));
  }
}
